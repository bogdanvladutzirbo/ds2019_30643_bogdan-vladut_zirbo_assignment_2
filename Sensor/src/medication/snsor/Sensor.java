package medication.snsor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Sensor {

	private static String fileName = "C:\\Users\\Vlad\\Desktop\\activity.txt";
	private static final String QUEUE_NAME="sensor-queue";
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static int NO_OF_PATIENTS=2;
	private static int SECONDS=1;

	public static void main(String[] args) throws IOException, InterruptedException, ParseException {
		System.out.println("Sensor start");
		
		Random random=new Random();
		File file = new File(fileName);
		BufferedReader br = new BufferedReader(new FileReader(file));
		
		// Queue
		ConnectionFactory factory = new ConnectionFactory();
		
		Channel channel = null;
		try(Connection connection = factory.newConnection()) {
			channel = connection.createChannel();
 			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
 			
 			String st;
 			while ((st = br.readLine()) != null) {

 				String[] s = st.split("\\t+");
 				String data=st;
 				Date start=sdf.parse(s[0]);
 				Date end=sdf.parse(s[1]);
 					
 				JSONObject json=new JSONObject();
 				int id=random.nextInt(2)+1;
 				json.put("patient_id",String.valueOf(id) );
 				json.put("activity", s[2]);
 				json.put("start", start.getTime());
 				json.put("end", end.getTime());
 				data=json.toString();
 				System.out.println("I sent:"+ data);
 				channel.basicPublish("", QUEUE_NAME, false, null, data.getBytes());
 				TimeUnit.SECONDS.sleep(SECONDS);
 			}
 			System.out.println("Sensor finish!");
		} catch (TimeoutException e) {
			e.printStackTrace();
		}

		
	}
}
