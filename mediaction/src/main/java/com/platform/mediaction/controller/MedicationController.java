package com.platform.mediaction.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.platform.mediaction.dto.MedicationDTO;
import com.platform.mediaction.service.MedicationService;

@RestController
@CrossOrigin
@RequestMapping("/medication")
public class MedicationController {

	private MedicationService medicationService;

	@Autowired
	public MedicationController(MedicationService medicationService) {
		this.medicationService = medicationService;
	}

	@GetMapping("/all")
	public List<MedicationDTO> getAll() {
		return medicationService.getAll();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public Integer add(@RequestBody MedicationDTO dto) {
		return medicationService.add(dto);
	}
	@PutMapping("/update")
	public Integer update(@Valid @RequestBody MedicationDTO dto) {
		return medicationService.update(dto);
	}
	
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") Integer id) {
		medicationService.delete(id);
	}
}
