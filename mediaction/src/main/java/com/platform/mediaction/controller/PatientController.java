package com.platform.mediaction.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.platform.mediaction.dto.PatientDTO;
import com.platform.mediaction.dto.PatientViewDTO;
import com.platform.mediaction.service.PatientService;

@RestController
@CrossOrigin
@RequestMapping("/patient")
public class PatientController {

	private PatientService patientService;

	@Autowired
	public PatientController(PatientService patientService) {
		this.patientService = patientService;
	}

	@GetMapping("/all")
	public List<PatientViewDTO> getAllPatients() {
		return patientService.getAllPatients();
	}

	@GetMapping("/{id}")
	public PatientViewDTO getPatient(@PathVariable("id") Integer id) {
		return patientService.getPatientById(id);
	}

	@PutMapping("/update")
	public Integer update(@Valid @RequestBody PatientViewDTO patient) {
		return patientService.updatePatient(patient);
	}

	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") Integer id) {
		patientService.deletePatient(id);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public Integer add(@RequestBody PatientDTO patient) {
		return patientService.addPatient(patient);
	}

}
