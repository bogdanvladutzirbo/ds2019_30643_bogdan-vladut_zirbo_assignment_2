package com.platform.mediaction.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.platform.mediaction.dto.DoctorDTO;
import com.platform.mediaction.dto.DoctorViewDTO;
import com.platform.mediaction.service.DoctorService;

@RestController
@CrossOrigin
@RequestMapping("/doctor")
public class DoctorController {

	private DoctorService doctorService;

	@Autowired
	public DoctorController(DoctorService doctorService) {
		this.doctorService = doctorService;
	}

	@GetMapping("/all")
	public List<DoctorViewDTO> getAll() {
		return doctorService.getAll();
	}
	
	@PutMapping("/update")
	public Integer update(@Valid @RequestBody DoctorViewDTO doctor) {
		return doctorService.update(doctor);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public Integer save(@RequestBody DoctorDTO doctor) {
		return doctorService.registerDoctor(doctor);
	}
	
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") Integer id) {
		doctorService.deleteDoctor(id);
	}
	
}
