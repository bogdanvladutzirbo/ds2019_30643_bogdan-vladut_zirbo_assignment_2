package com.platform.mediaction.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.platform.mediaction.dto.CaregiverDTO;
import com.platform.mediaction.dto.CaregiverViewDTO;
import com.platform.mediaction.dto.PatientViewDTO;
import com.platform.mediaction.service.CaregiverService;

@RestController
@CrossOrigin
@RequestMapping("/caregiver")
public class CaregiverController {

	private CaregiverService caregiverService;

	@Autowired
	public CaregiverController(CaregiverService caregiverService) {
		this.caregiverService = caregiverService;
	}

	@GetMapping("/all")
	public List<CaregiverViewDTO> getAll() {
		return caregiverService.getAllCaregivers();
	}

	@GetMapping("/associated/{id}")
	public List<PatientViewDTO> getAssociated(@PathVariable Integer id) {
		return caregiverService.getAllAssociatedPatients(id);
	}

	@PutMapping("/update")
	public Integer update(@Valid @RequestBody CaregiverViewDTO dto) {
		return caregiverService.update(dto);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public Integer add(@RequestBody CaregiverDTO caregiver) {
		return caregiverService.save(caregiver);
	}

	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") Integer id) {
		caregiverService.deleteCaregiver(id);
	}
}
