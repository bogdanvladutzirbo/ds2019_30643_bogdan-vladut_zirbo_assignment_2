package com.platform.mediaction.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@Controller
public class WebSocketController {

	private SimpMessagingTemplate tem;

	@Autowired
	public WebSocketController(SimpMessagingTemplate tem) {
		this.tem = tem;
	}

	@MessageMapping("/send/message")
	public void onReceieMessage(String message) {
		tem.convertAndSend("/chat", new SimpleDateFormat("HH:mm:ss").format(new Date()) + " " + message);
	}
}
