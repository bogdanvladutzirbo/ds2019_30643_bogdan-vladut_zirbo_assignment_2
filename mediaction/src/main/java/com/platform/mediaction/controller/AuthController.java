package com.platform.mediaction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.platform.mediaction.dto.AuthDTO;
import com.platform.mediaction.dto.UserViewDTO;
import com.platform.mediaction.service.AuthService;

@RestController
@CrossOrigin
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	public AuthService authService;

	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public UserViewDTO auth(@RequestBody AuthDTO auth) {
		return authService.getCredentials(auth);
	}

}
