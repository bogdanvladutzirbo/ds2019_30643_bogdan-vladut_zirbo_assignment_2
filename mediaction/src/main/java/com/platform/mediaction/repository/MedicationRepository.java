package com.platform.mediaction.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.platform.mediaction.model.Medication;

public interface MedicationRepository extends JpaRepository<Medication,Integer>{

}
