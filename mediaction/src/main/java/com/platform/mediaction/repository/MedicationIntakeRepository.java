package com.platform.mediaction.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.platform.mediaction.model.MedicationIntake;

public interface MedicationIntakeRepository extends JpaRepository<MedicationIntake, Integer>{
	@Query("SELECT m FROM MedicationIntake m WHERE m.idPatient=:id")
	List<MedicationIntake> getPatientMed(@Param("id")Integer id);
}
