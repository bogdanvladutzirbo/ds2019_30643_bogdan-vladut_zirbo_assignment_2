package com.platform.mediaction.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.platform.mediaction.model.Doctor;

public interface DoctorRepository extends JpaRepository<Doctor, Integer>{

}
