package com.platform.mediaction.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.platform.mediaction.model.Activity;

public interface ActivityRepository extends JpaRepository<Activity, Integer>{

}
