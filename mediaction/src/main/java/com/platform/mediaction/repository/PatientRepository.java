package com.platform.mediaction.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.platform.mediaction.model.Patient;

public interface PatientRepository extends JpaRepository<Patient,Integer>{

}
