package com.platform.mediaction.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.platform.mediaction.model.Caregiver;
import com.platform.mediaction.model.Patient;

public interface CaregiverRepository extends JpaRepository<Caregiver,Integer>{
	/*
	 * @Query("SELECT u.name FROM User u WHERE u.email=:email")
	public String getName(@Param("email") String email);
	 */
	@Query("SELECT p FROM Patient p  WHERE p.idCaregiver IN (SELECT c.idCaregiver FROM Caregiver c where c.idUser=:id)")
	List<Patient> getAllAssociatedPatient(@Param("id")Integer id);
}
