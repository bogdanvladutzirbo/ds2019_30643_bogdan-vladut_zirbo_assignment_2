package com.platform.mediaction.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.platform.mediaction.dto.CaregiverDTO;
import com.platform.mediaction.dto.CaregiverViewDTO;
import com.platform.mediaction.dto.PatientViewDTO;
import com.platform.mediaction.model.Caregiver;
import com.platform.mediaction.model.Patient;
import com.platform.mediaction.model.User;
import com.platform.mediaction.repository.CaregiverRepository;
import com.platform.mediaction.repository.UserRepository;

@Service
public class CaregiverService {

	@Autowired
	public CaregiverRepository caregiverRepository;

	@Autowired
	UserRepository userRepository;

	public List<PatientViewDTO> getAllAssociatedPatients(Integer id) {
		List<PatientViewDTO> associatedPatients = new LinkedList<PatientViewDTO>();

		List<Patient> patients = caregiverRepository.getAllAssociatedPatient(id);

		for (Patient p : patients) {
			User u = userRepository.getOne(p.getIdUser());
			PatientViewDTO dto = new PatientViewDTO(p.getIdPatient(), u.getName(), u.getBirthDate(), u.getGender(),
					u.getAddress(), u.getPhone(), p.getMedicalRecord(), p.getIdCaregiver());
			associatedPatients.add(dto);
		}
		return associatedPatients;
	}

	public List<CaregiverViewDTO> getAllCaregivers() {
		List<CaregiverViewDTO> list = new LinkedList<CaregiverViewDTO>();
		List<Caregiver> caregivers = caregiverRepository.findAll();
		for (Caregiver c : caregivers) {
			User u = userRepository.getOne(c.getIdUser());
			CaregiverViewDTO dto = new CaregiverViewDTO(c.getIdCaregiver(), u.getName(), u.getBirthDate(),
					u.getGender(), u.getAddress(), u.getPhone(), c.getIdDoctor());
			list.add(dto);
		}
		return list;
	}

	public void deleteCaregiver(Integer id) {
		caregiverRepository.deleteById(id);

	}

	public Integer update(CaregiverViewDTO caregiver) {
		Integer result = -1;
		Caregiver c = caregiverRepository.getOne(caregiver.getId());
		User u = userRepository.getOne(c.getIdUser());
		c.setIdDoctor(caregiver.getIdDoctor());

		u.setName(caregiver.getName());
		u.setBirthDate(caregiver.getBirthDate());
		u.setGender(caregiver.getGender());
		u.setAddress(caregiver.getAddress());
		u.setPhone(caregiver.getPhone());
		userRepository.save(u);
		caregiverRepository.save(c);
		result = caregiver.getId();

		return result;
	}

	public Integer save(CaregiverDTO dto) {
		Caregiver c = new Caregiver();
		User u = new User();
		u.setName(dto.getName());
		u.setBirthDate(dto.getBirthDate());
		u.setGender(dto.getGender());
		u.setAddress(dto.getAddress());
		u.setPhone(dto.getPhone());
		u.setUsername(dto.getUsername());
		u.setPassword(dto.getPassword());
		u.setRole(dto.getRole());
		Integer id = userRepository.save(u).getId();

		c.setIdUser(id);
		c.setIdDoctor(dto.getIdDoctor());

		return caregiverRepository.save(c).getIdCaregiver();
	}

}
