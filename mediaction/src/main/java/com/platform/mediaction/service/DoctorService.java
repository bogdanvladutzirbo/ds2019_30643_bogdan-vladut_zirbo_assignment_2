package com.platform.mediaction.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.platform.mediaction.dto.DoctorDTO;
import com.platform.mediaction.dto.DoctorViewDTO;
import com.platform.mediaction.model.Doctor;
import com.platform.mediaction.model.User;
import com.platform.mediaction.repository.DoctorRepository;
import com.platform.mediaction.repository.UserRepository;

@Service
public class DoctorService {

	@Autowired
	public DoctorRepository doctorRepository;

	@Autowired
	public UserRepository userRepository;

	public Integer registerDoctor(DoctorDTO doctor) {
		User u = new User();
		u.setName(doctor.getName());
		u.setBirthDate(doctor.getBirthDate());
		u.setGender(doctor.getGender());
		u.setAddress(doctor.getAddress());
		u.setPhone(doctor.getPhone());
		u.setUsername(doctor.getUsername());
		u.setPassword(doctor.getPassword());
		u.setRole(doctor.getRole());
		Integer idUser = userRepository.save(u).getId();
		Doctor d = new Doctor();
		d.setIdUser(idUser);
		return doctorRepository.save(d).getIdDoctor();
	}

	public void deleteDoctor(Integer id) {
		doctorRepository.deleteById(id);
	}

	public List<DoctorViewDTO> getAll() {
		List<DoctorViewDTO> list = new LinkedList<>();
		List<Doctor> doctors = doctorRepository.findAll();
		for (Doctor d : doctors) {
			User u = userRepository.getOne(d.getIdUser());
			DoctorViewDTO dto = new DoctorViewDTO(d.getIdDoctor(), u.getName(), u.getBirthDate(), u.getGender(),
					u.getAddress(), u.getPhone());
			list.add(dto);

		}
		return list;
	}

	public Integer update(DoctorViewDTO doctor) {
		Doctor d = doctorRepository.getOne(doctor.getId());
		User u = userRepository.getOne(d.getIdUser());
		u.setName(doctor.getName());
		u.setBirthDate(doctor.getBirthDate());
		u.setGender(doctor.getGender());
		u.setAddress(doctor.getAddress());
		u.setPhone(doctor.getPhone());
		userRepository.save(u);
		return d.getIdDoctor();
	}

	public String getTest() {
		return "test works!";
	}
}
