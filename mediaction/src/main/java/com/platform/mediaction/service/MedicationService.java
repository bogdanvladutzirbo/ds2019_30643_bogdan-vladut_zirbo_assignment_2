package com.platform.mediaction.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.platform.mediaction.dto.MedicationDTO;
import com.platform.mediaction.model.Medication;
import com.platform.mediaction.repository.MedicationRepository;

@Service
public class MedicationService {

	@Autowired
	public MedicationRepository medicationRepository;

	public Integer add(MedicationDTO dto) {
		Medication medication = new Medication(dto.getId(), dto.getName(), dto.getInfo(), dto.getSideEffects());
		return medicationRepository.save(medication).getId();
	}

	public void delete(Integer id) {
		medicationRepository.deleteById(id);
	}

	public List<MedicationDTO> getAll() {
		List<Medication> list = medicationRepository.findAll();
		List<MedicationDTO> dtos = new LinkedList<>();
		for (Medication m : list) {
			MedicationDTO dto = new MedicationDTO(m.getId(), m.getName(), m.getInfo(), m.getSideEffects());
			dtos.add(dto);
		}

		return dtos;
	}

	public MedicationDTO getById(Integer id) {
		Medication m = medicationRepository.getOne(id);
		MedicationDTO dto = new MedicationDTO(m.getId(), m.getName(), m.getInfo(), m.getSideEffects());
		return dto;
	}

	public Integer update(MedicationDTO dto) {
		Medication m = medicationRepository.getOne(dto.getId());
		
		m.setInfo(dto.getInfo());
		m.setName(dto.getName());
		m.setSideEffects(dto.getSideEffects());
		return medicationRepository.save(m).getId();
	}
}
