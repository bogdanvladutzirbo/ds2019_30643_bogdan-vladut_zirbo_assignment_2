package com.platform.mediaction.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.platform.mediaction.dto.MedicationIntakeDTO;
import com.platform.mediaction.model.MedicationIntake;
import com.platform.mediaction.repository.MedicationIntakeRepository;

public class MedicationIntakeService {

	@Autowired
	public MedicationIntakeRepository repository;

	public List<MedicationIntakeDTO> getAll() {
		List<MedicationIntake> medication = repository.findAll();
		List<MedicationIntakeDTO> list = new LinkedList<MedicationIntakeDTO>();
		for (MedicationIntake i : medication) {
			MedicationIntakeDTO dto = new MedicationIntakeDTO(i.getId(), i.getIdMed(), i.getIdPatient(), i.getStart(),
					i.getEnd());
			list.add(dto);
		}
		return list;
	}

	public List<MedicationIntakeDTO> getPatientMed(Integer id) {
		List<MedicationIntake> m = repository.getPatientMed(id);
		List<MedicationIntakeDTO> list = new LinkedList<MedicationIntakeDTO>();

		for (MedicationIntake i : m) {
			MedicationIntakeDTO dto = new MedicationIntakeDTO(i.getId(), i.getIdMed(), i.getIdPatient(), i.getStart(),
					i.getEnd());
			list.add(dto);
		}
		return list;
	}

	public void delete(Integer id) {
		repository.deleteById(id);
	}

	public Integer add(MedicationIntakeDTO dto) {
		MedicationIntake m = new MedicationIntake();
		m.setEnd(dto.getEnd());
		m.setIdMed(dto.getIdMed());
		m.setIdPatient(dto.getIdPatient());
		m.setStart(dto.getStart());
		return repository.save(m).getId();
	}

	public Integer update(MedicationIntakeDTO dto) {
		MedicationIntake m = repository.getOne(dto.getId());
		m.setEnd(dto.getEnd());
		m.setIdMed(dto.getIdMed());
		m.setIdPatient(dto.getIdPatient());
		m.setStart(dto.getStart());
		return repository.save(m).getId();
	}
}
