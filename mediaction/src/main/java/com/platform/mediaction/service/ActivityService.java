package com.platform.mediaction.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.platform.mediaction.dto.ActivityDTO;
import com.platform.mediaction.model.Activity;
import com.platform.mediaction.repository.ActivityRepository;

@Service
public class ActivityService {

	@Autowired
	public ActivityRepository activityRepository;

	public Integer addActivity(ActivityDTO dto) {
		Activity activity=new Activity();
		activity.setPatientId(dto.getPatientId());
		activity.setActivity(dto.getActivity());
		activity.setStart(dto.getStart());
		activity.setEnd(dto.getEnd());
		return activityRepository.save(activity).getId();
	}
	
}
