package com.platform.mediaction.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.platform.mediaction.dto.AuthDTO;
import com.platform.mediaction.dto.UserViewDTO;
import com.platform.mediaction.errorhendler.NotFoundException;
import com.platform.mediaction.model.User;
import com.platform.mediaction.repository.UserRepository;

@Service
public class AuthService {

	@Autowired
	public UserRepository userRepository;

	public UserViewDTO getCredentials(AuthDTO dto) {
		User u = userRepository.getCredentials(dto.getUsername(), dto.getPassword());
		UserViewDTO obj = null;
		if (u != null) {
			obj = new UserViewDTO(u.getId(), u.getName(), u.getBirthDate(), u.getGender(), u.getAddress(), u.getPhone(),
					u.getRole());
		} else {
			throw new NotFoundException("Username: " + dto.getUsername());
		}

		return obj;
	}

}
