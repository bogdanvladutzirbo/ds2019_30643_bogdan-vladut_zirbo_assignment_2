package com.platform.mediaction.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.platform.mediaction.dto.PatientDTO;
import com.platform.mediaction.dto.PatientViewDTO;
import com.platform.mediaction.model.Patient;
import com.platform.mediaction.model.User;
import com.platform.mediaction.repository.PatientRepository;
import com.platform.mediaction.repository.UserRepository;

@Service
public class PatientService {

	@Autowired
	public PatientRepository patientRepository;

	@Autowired
	public UserRepository userRepository;

	public List<PatientViewDTO> getAllPatients() {
		List<Patient> patients = patientRepository.findAll();
		List<PatientViewDTO> dtos = new LinkedList<>();
		for (Patient p : patients) {
			PatientViewDTO dto = new PatientViewDTO();
			User u = userRepository.getOne(p.getIdUser());
			dto.setId(p.getIdPatient());
			dto.setName(u.getName());
			dto.setBirthDate(u.getBirthDate());
			dto.setGender(u.getGender());
			dto.setAddress(u.getAddress());
			dto.setPhone(u.getPhone());
			dto.setMedicalRecord(p.getMedicalRecord());
			dto.setIdCaregiver(p.getIdCaregiver());
			dtos.add(dto);
		}
		return dtos;
	}

	public PatientViewDTO getPatientById(Integer id) {
		PatientViewDTO dto = null;
		Patient p = patientRepository.getOne(id);
		if (p != null) {
			User u = userRepository.getOne(p.getIdUser());
			dto = new PatientViewDTO(p.getIdPatient(), u.getName(), u.getBirthDate(), u.getGender(), u.getAddress(),
					u.getPhone(), p.getMedicalRecord(), p.getIdCaregiver());
		}
		return dto;
	}

	public void deletePatient(Integer id) {
		patientRepository.deleteById(id);
	}

	public Integer addPatient(PatientDTO user) {
		Integer idPatient = -1;
		User u = new User();
		Patient p = new Patient();

		u.setName(user.getName());
		u.setBirthDate(user.getBirthDate());
		u.setGender(user.getGender());
		u.setUsername(user.getUsername());
		u.setAddress(user.getAddress());
		u.setPhone(user.getPhone());
		u.setPassword(user.getPassword());
		u.setRole(user.getRole());
		idPatient = userRepository.save(u).getId();

		p.setIdCaregiver(user.getIdCaregiver());
		p.setIdUser(idPatient);
		p.setMedicalRecord("mdr_" + String.valueOf(idPatient));

		return patientRepository.save(p).getIdPatient();
	}

	public Integer updatePatient(PatientViewDTO patient) {
		Integer result = -1;
		Patient p = patientRepository.getOne(patient.getId());
		User u = userRepository.getOne(p.getIdUser());

		p.setIdCaregiver(patient.getIdCaregiver());
		u.setAddress(patient.getAddress());
		u.setBirthDate(patient.getBirthDate());
		u.setGender(patient.getGender());
		u.setName(patient.getName());
		u.setPhone(patient.getPhone());
		userRepository.save(u);
		result = patientRepository.save(p).getIdPatient();
		return result;
	}
}
