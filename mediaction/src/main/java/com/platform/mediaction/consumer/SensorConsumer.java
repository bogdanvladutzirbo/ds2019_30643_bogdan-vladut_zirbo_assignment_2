package com.platform.mediaction.consumer;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.util.concurrent.TimeoutException;

import javax.annotation.PostConstruct;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.platform.mediaction.dto.ActivityDTO;
import com.platform.mediaction.service.ActivityService;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

@Component
public class SensorConsumer {
	
	@Autowired
	public ActivityService activityService;
	
	private static final String QUEUE_NAME = "sensor-queue";
	// private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd
	// HH:mm:ss");
	@PostConstruct
	public void consume() throws IOException, TimeoutException {
		System.out.println("Consumer start!");
		ConnectionFactory factory = new ConnectionFactory();
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		System.out.println("Waiting...");

		DeliverCallback deliverCallback = (consumerTag, delivery) -> {
			String message = new String(delivery.getBody(), "UTF-8");

			try {
				processMessage(message);
			} catch (JSONException | ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
		});

	}

	private void processMessage(String msg) throws JSONException, ParseException {
		JSONObject json = new JSONObject(msg);
		Integer patientId = Integer.parseInt(json.getString("patient_id"));
		String activity = json.getString("activity");
		Date start = new Date(json.getLong("start"));
		Date end = new Date(json.getLong("end"));
		ActivityDTO a = new ActivityDTO(patientId, activity, start, end);
		int rule = checkRules(activity, start, end);
		if(rule>0) {
			if(activityService==null) {
				System.out.println("Service!");
			}
			else if(a==null) {
				System.out.println("Activity!");
			}
			activityService.addActivity(a);
		}
		System.out.println(a.toString() + "|" + rule);
	}

	private int checkRules(String activity, Date start, Date end) {
		int result = 0;
		long dif = end.getTime() - start.getTime();
		dif = dif / (60 * 60 * 1000);// hours
		if ("Sleeping".equals(activity)) {
			if (dif > 12) {
				System.out.println("R1: The sleep period longer than 12 hours");
				result = 1;
			}
		} else if ("Leaving".equals(activity)) {
			if (dif > 12) {
				System.out.println("R2: The leaving activity (outdoor) is longer than 12 hours");
				result = 2;
			}
		} else if ("Toileting".equals(activity) || "Showering".equals(activity)) {
			if (dif > 1) {
				System.out.println("R3: The period spent in bathroom is longer than 1 hour");
				result = 3;
			}
		}
		return result;
	}
}
