package com.platform.mediaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MediactionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediactionApplication.class, args);
	}

}
