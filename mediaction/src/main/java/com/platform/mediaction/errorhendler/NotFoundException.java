package com.platform.mediaction.errorhendler;

public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String NOT_FOUND = "ERROR: Not found Exception: ";

	public NotFoundException(String msg) {
		super(NOT_FOUND + msg +"!");
	}
}
