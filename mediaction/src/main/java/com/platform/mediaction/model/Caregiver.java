package com.platform.mediaction.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@Table(name = "caregiver")
public class Caregiver {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idCaregiver;
	private Integer idUser;
	private Integer idDoctor;

	public Integer getIdCaregiver() {
		return idCaregiver;
	}

	public void setIdCaregiver(Integer idCaregiver) {
		this.idCaregiver = idCaregiver;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public Integer getIdDoctor() {
		return idDoctor;
	}

	public void setIdDoctor(Integer idDoctor) {
		this.idDoctor = idDoctor;
	}
}
