package com.platform.mediaction.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@Table(name = "patient")
public class Patient {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPatient;
	private Integer idUser;
	private Integer idCaregiver;
	private String medicalRecord;

	public Patient(Integer idPatient, Integer idUser, Integer idCaregiver, String medicalRecord) {
		super();
		this.idPatient = idPatient;
		this.idUser = idUser;
		this.idCaregiver = idCaregiver;
		this.medicalRecord = medicalRecord;
	}

	public Patient() {
	}

	public Integer getIdPatient() {
		return idPatient;
	}

	public void setIdPatient(Integer idPatient) {
		this.idPatient = idPatient;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public Integer getIdCaregiver() {
		return idCaregiver;
	}

	public void setIdCaregiver(Integer idCaregiver) {
		this.idCaregiver = idCaregiver;
	}

	public String getMedicalRecord() {
		return medicalRecord;
	}

	public void setMedicalRecord(String medicalRecord) {
		this.medicalRecord = medicalRecord;
	}

}
