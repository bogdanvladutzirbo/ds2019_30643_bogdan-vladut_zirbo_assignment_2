package com.platform.mediaction.dto;

import java.sql.Date;

public class ActivityDTO {
	private Integer patientId;
	private String activity;
	private Date start;
	private Date end;

	public ActivityDTO() {
	}

	public ActivityDTO(Integer patientId, String activity, Date start, Date end) {
		super();
		this.patientId = patientId;
		this.activity = activity;
		this.start = start;
		this.end = end;
	}

	public Integer getPatientId() {
		return patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	@Override
	public String toString() {
		return "Activity [patientId=" + patientId + ", activity=" + activity + ", start=" + start + ", end=" + end
				+ "]";
	}
}
