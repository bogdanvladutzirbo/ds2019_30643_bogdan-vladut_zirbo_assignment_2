package com.platform.mediaction.dto;

public class MedicationDTO {
	private Integer id;
	private String name;
	private String info;
	private String sideEffects;

	public MedicationDTO() {
	}

	public MedicationDTO(Integer id, String name, String info, String sideEffects) {
		super();
		this.id = id;
		this.name = name;
		this.info = info;
		this.sideEffects = sideEffects;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getSideEffects() {
		return sideEffects;
	}

	public void setSideEffects(String sideEffects) {
		this.sideEffects = sideEffects;
	}

}
