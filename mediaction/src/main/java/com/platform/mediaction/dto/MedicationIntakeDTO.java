package com.platform.mediaction.dto;

import java.sql.Date;

public class MedicationIntakeDTO {
	private Integer id;
	private Integer idMed;
	private Integer idPatient;
	private Date start;
	private Date end;

	public MedicationIntakeDTO() {
	}

	public MedicationIntakeDTO(Integer id, Integer idMed, Integer idPatient, Date start, Date end) {
		super();
		this.id = id;
		this.idMed = idMed;
		this.idPatient = idPatient;
		this.start = start;
		this.end = end;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdMed() {
		return idMed;
	}

	public void setIdMed(Integer idMed) {
		this.idMed = idMed;
	}

	public Integer getIdPatient() {
		return idPatient;
	}

	public void setIdPatient(Integer idPatient) {
		this.idPatient = idPatient;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

}
