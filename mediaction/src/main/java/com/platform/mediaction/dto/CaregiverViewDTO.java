package com.platform.mediaction.dto;

import java.sql.Date;

public class CaregiverViewDTO {

	private Integer id;
	private String name;
	private Date birthDate;
	private String gender;
	private String address;
	private String phone;
	private Integer idDoctor;

	public CaregiverViewDTO() {
	}

	public CaregiverViewDTO(Integer id, String name, Date birthDate, String gender, String address, String phone,
			Integer idDoctor) {
		super();
		this.id = id;
		this.name = name;
		this.birthDate = birthDate;
		this.gender = gender;
		this.address = address;
		this.phone = phone;
		this.idDoctor = idDoctor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "CaregiverViewDTO [id=" + id + ", name=" + name + ", birthDate=" + birthDate + ", gender=" + gender
				+ ", address=" + address + ", phone=" + phone + ", idDoctor=" + idDoctor + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getIdDoctor() {
		return idDoctor;
	}

	public void setIdDoctor(Integer idDoctor) {
		this.idDoctor = idDoctor;
	}

}
