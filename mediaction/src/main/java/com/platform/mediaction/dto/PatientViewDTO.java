package com.platform.mediaction.dto;

import java.sql.Date;

public class PatientViewDTO {
	private Integer id;
	private String name;
	private Date birthDate;
	private String gender;
	private String address;
	private String phone;
	private String medicalRecord;
	private Integer idCaregiver;

	public PatientViewDTO(Integer id, String name, Date birthDate, String gender, String address, String phone,
			String medicalRecord, Integer idCaregiver) {
		super();
		this.id = id;
		this.name = name;
		this.birthDate = birthDate;
		this.gender = gender;
		this.address = address;
		this.phone = phone;
		this.medicalRecord = medicalRecord;
		this.idCaregiver = idCaregiver;
	}
	
	public PatientViewDTO() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMedicalRecord() {
		return medicalRecord;
	}

	public void setMedicalRecord(String medicalRecord) {
		this.medicalRecord = medicalRecord;
	}

	public Integer getIdCaregiver() {
		return idCaregiver;
	}

	public void setIdCaregiver(Integer idCaregiver) {
		this.idCaregiver = idCaregiver;
	}

	
}
